class_name Spawner
extends Area2D


@export var PV_max = 100.0
var PV = PV_max
@export var scale_max = Vector2(1.0,1.0)
enum camp {LOVER,HATER}
var state = camp.HATER
var nb_ennemis = 0
var lover = preload("res://Characters/lover.tscn")
var hater = preload("res://Characters/hater.tscn")
@export var nb_ennemis_max = 50


func _ready():
	$TimerSpawn.start()
	$TrouNoir.get("material").set_shader_parameter("col",Color.WHITE)
	

func _process(delta):
	scale = (PV/PV_max)*scale_max
	


func _on_timer_spawn_timeout():
	if(nb_ennemis<nb_ennemis_max):
		var new_child
		if(state==camp.HATER):
			new_child = hater.instantiate()
		else:
			new_child = lover.instantiate()
		new_child.change_state(new_child.State.DEFEND)
		var limit = $Detection.shape.radius
		var _x = randi_range(-limit,limit)
		var _y = randi_range(-limit,limit)
		new_child.position= to_global(Vector2(float(_x),float(_y)))
		get_parent().add_child(new_child)
		nb_ennemis+=1
	if(state==camp.HATER):
		if(PV<PV_max):
			PV+=10



func _on_area_entered(area):
	if state==camp.HATER and area is Projectile or (area.name=="HurtBox" and area.get_parent() is Lover):
		area.queue_free()
		PV-=10
	if(state==camp.HATER and PV<=0):
		state=camp.LOVER
		PV=100
		nb_ennemis=0
		$TrouNoir.get("material").set_shader_parameter("col",Color("d1559d"))
