extends Area2D
class_name Projectile

const SPEED = 800.0

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("area_entered", func(area): print("collision w/ area: ", area.name, " ", area))
	connect("body_entered", func(body): print("collision w/ body: ", body.name, " ", body))


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	global_position += transform.x * SPEED * delta
#	position.x =	transform.x 


