extends Node2D

var trigger_action : StringName = "ui_accept"


func _unhandled_input(event):
	pass
	if event.is_action_pressed(trigger_action) and $TimeLimit.is_stopped():
		var alea = randi() % 3
		var proj = preload("res://Elements/projectile.tscn").instantiate()
		get_tree().get_current_scene().add_child(proj)
#		proj.global_transform = $SpawnPosition.global_transform.orthonormalized() ?
		proj.global_transform = $SpawnPosition.global_transform
		#Car TimeLimit n'est jamais start de ce que je vois
		$Shot_0.play()
		$TimeLimit.start();
		if alea == 0: 
			$Shot_0.play()
			$Shot_1.stop()
			$Shot_2.stop()
		if alea == 1: 
			$Shot_0.stop()
			$Shot_1.play()
			$Shot_2.stop()
		if alea == 2: 
			$Shot_0.stop()
			$Shot_1.stop()
			$Shot_2.play()
			

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
