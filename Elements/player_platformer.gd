@tool
extends CharacterBody2D


const SPEED = 400.0
const JUMP_VELOCITY = 800.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = 980 * 1.3

var can_jump = false
var is_jump = false
var local_orthogonal = Vector2.ZERO

@export var invert_left_right = false
@export var use_updown_instead_leftright = false

func _ready():
	if not Engine.is_editor_hint():
		up_direction = up_direction.normalized()

func _physics_process(delta):
	if Engine.is_editor_hint():
		return
	up_direction = up_direction.normalized()
	can_jump = ( is_on_floor() or not $Coyote.is_stopped() ) and not is_jump
	
	var is_in_air = not is_on_floor()
	if is_in_air:
		# Apply gravity.
		local_orthogonal.y += gravity * delta
	else:
		$Coyote.start()
		is_jump = false

	# Handle Jump.
	if Input.is_action_just_pressed("sauter") and can_jump:
		local_orthogonal.y = -JUMP_VELOCITY
		is_jump = true

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("left", "right")
	if use_updown_instead_leftright:
		direction = Input.get_axis("up", "down")
	if direction:
		local_orthogonal.x = direction * SPEED
	else:
		local_orthogonal.x = move_toward(local_orthogonal.x, 0, SPEED)

	velocity = local_orthogonal.y * -up_direction
	var orthogonal_x = -up_direction.orthogonal() if not invert_left_right else up_direction.orthogonal()
	velocity += local_orthogonal.x * orthogonal_x
	move_and_slide()

func _process(delta):
	if Engine.is_editor_hint():
		for c in get_children():
			if "show_behind_parent" in c:
				c.show_behind_parent = true
		queue_redraw()

func _draw():
	if Engine.is_editor_hint() or get_tree().debug_collisions_hint:
		self_modulate.a = .6
		draw_line(Vector2.ZERO, -up_direction * SPEED, Color.RED, 5.0)
		
#		draw_line(up_direction * SPEED, -up_direction * SPEED - Vector2.ONE * 5.0, Color.RED, 3.0)

		#Works but use a percentage of the vector instead of a fixed number of pixel for the tip of the arrow on the length of the vector
#		draw_line(up_direction * SPEED, (-up_direction * SPEED * .95) - (-up_direction * SPEED).normalized().orthogonal() * 15.0, Color.RED, 3.0)
		draw_line(-up_direction * SPEED, (-up_direction * SPEED) - (-up_direction * SPEED).normalized() * 15.0 - (-up_direction * SPEED).normalized().orthogonal() * 15.0, Color.RED, 3.0)
		
		draw_line(-up_direction * SPEED, (-up_direction * SPEED) - (-up_direction * SPEED).normalized() * 15.0 + (-up_direction * SPEED).normalized().orthogonal() * 15.0, Color.RED, 3.0)
		
		draw_string(ThemeDB.fallback_font, (-up_direction * SPEED)/2.0 - (-up_direction * SPEED).normalized().orthogonal() * 15.0, "1sec")

