extends Control
class_name Droppable

var current_draggable = null
var cur_draggable_preview = null

func _can_drop_data(at_position, data):
	return data is Draggable

func _drop_data(at_position, data):
	print(data)
	current_draggable = data
	if cur_draggable_preview:
		cur_draggable_preview.queue_free()
	var c_tex = TextureRect.new()
	c_tex.texture = data.drag_texture
	c_tex.position = at_position
	add_child(c_tex)
	cur_draggable_preview = c_tex
