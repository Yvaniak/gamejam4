@tool
extends Control
class_name Draggable

var drag_texture = preload("res://icon.svg")

func _init():
	pass

func _ready():
	pass

func _drop_data(at_position, data):
	pass

func _get_drag_data(_at_position):
	if Engine.is_editor_hint():
		return
	var c_tex = TextureRect.new()
	c_tex.texture = drag_texture
	set_drag_preview(c_tex)
	return self

func _process(delta):
	if Engine.is_editor_hint():
		queue_redraw()

func _draw():
	if Engine.is_editor_hint():
		draw_rect(Rect2(position, size*scale), Color(0.4, 0.8, 0.4, 0.7))
