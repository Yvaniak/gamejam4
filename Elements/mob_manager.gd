extends Node
class_name MobManager

var mob_list = []
const MAX_MOB = 5000
var hater_nb = 50
var lover_nb = 50
var tick_timer

var spawn_limit = 400.0 #margin around visible screen
var despawn_limit = 450.0 #margin around visible screen

#NOTE : Current implementation will spawn enemies
#outside the map limits, we will need to specify them to
#avoid this.


# Called when the node enters the scene tree for the first time.
func _ready():
	tick_timer = Timer.new()
	tick_timer.wait_time = 1.0
	add_child(tick_timer)
	tick_timer.start()
	tick_timer.timeout.connect(_on_tick_timer_timeout)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	for m in mob_list:
		var limit_rect = get_viewport().get_visible_rect().grow(despawn_limit)
		if not limit_rect.has_point(m.global_position): #and not m.state == m.State.PURSUE:
			if m is Hater:
				hater_nb += 1
			elif m is Lover:
				lover_nb += 1
			m.queue_free()
			#needs to be called at the bottom of the game loop because if we do it while we are iterating the array it's not a good idea
			call_deferred("unregister_mob", m)


func register_mob(mob):
	if not mob in mob_list:
		mob_list.append(mob)


func unregister_mob(mob):
	if mob in mob_list:
		mob_list.erase(mob)
		#call_deffered here instead?


func _on_tick_timer_timeout():
	#2/3 chance to spawn something
	if randi() % 3 > 0:
		#2/3 chance to spawn a hater
		#maybe store the odds in a value and change it later
		if randi() % 3 > 0:
			spawn_hater()
		else:
			spawn_lover()


func get_new_spawn_point():
	var cam_offset = (get_viewport().get_camera_2d().get_screen_center_position() - get_viewport().get_visible_rect().size / 2)
	var spawn_rect = get_viewport().get_visible_rect().grow(spawn_limit)
	spawn_rect.position = cam_offset
	var spawn_point = cam_offset + Vector2.ONE * 5
	while get_viewport().get_visible_rect().has_point(spawn_point):
		var _x = randi() % int(spawn_rect.size.x)
		var _y = randi() % int(spawn_rect.size.y)
		_x += spawn_rect.position.x
		_y += spawn_rect.position.y
		spawn_point.x = _x
		spawn_point.y = _y
	return spawn_point


func spawn_hater():
	if hater_nb < 1 or mob_list.size() >= MAX_MOB:
		return
	var h = preload("res://Characters/hater.tscn").instantiate()
	get_tree().current_scene.add_child(h)
	register_mob(h)
	h.state = h.State.WANDER
	h.global_position = get_new_spawn_point()
	hater_nb -= 1


func spawn_lover():
	pass
#	if lover_nb < 1 or mob_list.size() >= MAX_MOB:
#		return
#	var l = preload("res://Characters/lover.tscn").instantiate()
#	get_tree().current_scene.add_child(l)
#	register_mob(l)
#	l.state = l.State.WANDER
#	l.global_position = get_new_spawn_point()
#	lover_nb -= 1
