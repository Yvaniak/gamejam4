extends CharacterBody2D
class_name Player

enum MOVE_TYPE {CLASSIC, PIVOT_ON_TURN, ORIENT_ON_MOUSE, FOLLOW_MOUSE_CLICK}

const SPEED = 500.0 #pixel per secs
const PIVOT_SPD = 100.0 #degrees per secs

var mouse_target_position = null

#@export var pivot_on_turn = false
@export var movement_type = MOVE_TYPE.CLASSIC
var trigger_action : StringName = "ui_accept"
@export var PV = 100 
var hit = 0

func _process(delta):
	$Sprite2D.global_position = global_position
	$AnimatedSprite2D.global_position = global_position
	$ProgressBar.global_position = global_position


func _physics_process(delta):

	var direction = Vector2.ZERO
	direction.x = Input.get_axis("left", "right")
	direction.y = Input.get_axis("up", "down")
	match movement_type:
		MOVE_TYPE.CLASSIC:
			velocity = direction.normalized() * SPEED
		MOVE_TYPE.PIVOT_ON_TURN:
			rotation_degrees += direction.x * PIVOT_SPD * delta
			#transform.origin += transform.basis_xform(Vector2.UP * direction.y * SPEED * delta)
			if direction.y:
				velocity = transform.basis_xform(Vector2.DOWN * direction.y * SPEED)
			else:
				velocity = velocity.move_toward(Vector2.ZERO, SPEED)
		MOVE_TYPE.ORIENT_ON_MOUSE:
			transform = transform.looking_at(get_global_mouse_position())
			rotation_degrees += 90.0
			if direction.y:
				velocity = transform.basis_xform(Vector2.DOWN * direction.y * SPEED)
			else:
				velocity = velocity.move_toward(Vector2.ZERO, SPEED)
		MOVE_TYPE.FOLLOW_MOUSE_CLICK:
			if mouse_target_position:
				transform = transform.looking_at(mouse_target_position)
				rotation_degrees += 90.0
				velocity = (mouse_target_position - global_position).normalized() * SPEED
				if (mouse_target_position - global_position).length() < SPEED * delta:
					velocity = (mouse_target_position - global_position)
					await get_tree().process_frame
					velocity=Vector2.ZERO
					mouse_target_position = null
				if is_on_wall():
					velocity=Vector2.ZERO
					mouse_target_position = null
	#		else:
	#			mouse_target_position = get_global_mouse_position() if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT) else null
			if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT):
				mouse_target_position = get_global_mouse_position()

	if PV <= 0:
		get_tree().reload_current_scene()

	move_and_slide()
	
var cache
var lover_collectees = []



func _on_hit_box_area_entered(area):
	if area is Spawner or (area.get_parent() is Hater and area.name=="HurtBox"):
		hit += 1 
		cache = area
		$HIT.start()
		if (area.get_parent() is Hater and area.name=="HurtBox"):
#			area.get_parent().get("AnimationPlayer").play("atk")
			pass

	

func _on_hit_box_area_exited(area):
	if area is Spawner or (area.get_parent() is Hater and area.name=="HurtBox"): 
		hit -= 1
		if(area==cache):
			cache=null

func _on_hit_timeout():
	if hit >= 1:
		PV -= 10 * hit
		$ProgressBar.value = PV
		mouse_target_position = null
		velocity = Vector2.ZERO
		if(cache!=null):
			transform = transform.looking_at(cache.position)
			rotation_degrees += 90.0
			velocity = (global_position - cache.global_position).normalized() * 800
			await get_tree().create_timer(0.1).timeout
			velocity=Vector2.ZERO
			$HIT.start()


func _on_collecte_box_area_entered(area):
	if (area.get_parent() is Lover and area.name=="HurtBox"):
		if area.get_parent() not in lover_collectees:
			lover_collectees.append(area.get_parent())
			area.get_parent().follow_player()
