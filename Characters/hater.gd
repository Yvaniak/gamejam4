extends StaticBody2D
class_name Hater

#Wander: move randomly around (spawned in middle map)
#Defend: move around defense point (spawner)
#Pursue: follow one to fight
enum State {WANDER, DEFEND, PURSUE, TARGET_PLAYER}
var state = State.WANDER

var last_player_seen
var last_lover_seen
var target

var speed = 80
var direction = Vector2.ZERO
var thinking_timer

@export var PV_max = 20.0
var PV = PV_max
var cpt_ennemis = 0

signal target_out_of_sight

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	$DetectionBox.connect("body_entered", Callable(self, "_on_body_detected"))
	$DetectionBox.connect("body_exited", Callable(self, "_on_body_exit_detection"))
	
	#TODO
	$HurtBox.connect("area_entered", Callable(self, "_on_hurtbox_area_detect"))
	$HurtBox.connect("area_exited", Callable(self, "_on_hurtbox_exit_detection"))
	
	target_out_of_sight.connect(_on_target_out_of_sight)
	
	thinking_timer = Timer.new()
	thinking_timer.one_shot = true
	add_child(thinking_timer)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	match state:
		State.WANDER:
			#TODO move randomly
			if can_think():
				var rng = randf_range(-PI, PI)
				direction = Vector2(cos(rng),sin(rng))
			position += direction * speed * delta
			look_for_someone_to_atk()
		State.DEFEND:
			#TODO move around the thing it is defending; staying near it
			look_for_someone_to_atk()
		State.PURSUE:
			position += (target.global_position - global_position).normalized() * speed * delta
		State.TARGET_PLAYER:
			position += (target.global_position - global_position).normalized() * speed * delta
	if(cpt_ennemis>0):
		if($AnimAttack.is_stopped()):
			$AnimationPlayer.play("atk")
			$AnimAttack.start()
	print("cpt",cpt_ennemis)
	print("timer",$AnimAttack.is_stopped())

func change_state(s : State):
	state = s


func _on_body_detected(body):
	if body is Player:
		last_player_seen = body
	elif body is Lover:
		last_lover_seen = body

func _on_body_exit_detection(body):
	if body is Player:
		if body == last_player_seen:
			last_player_seen = null
	elif body is Lover:
		if body == last_lover_seen:
			last_lover_seen = null
	if body == target:
		emit_signal("target_out_of_sight")


func _on_target_out_of_sight():
	if state == State.PURSUE:
		cancel_chase()


func look_for_someone_to_atk():
	#2/3 chance to target player over lover if lover is found too
	if last_player_seen:
		if (randi() % 3 > 0 and last_lover_seen) or not last_lover_seen:
			start_chase(last_player_seen)
	elif last_lover_seen:
		start_chase(last_lover_seen)
	

func start_chase(new_target):
	change_state(State.PURSUE)
	target = new_target

func cancel_chase():
	change_state(State.WANDER)
	target = null


func force_player_chase():
	change_state(State.TARGET_PLAYER)
	target = find_child("Player", true, false)


func can_think():
	if thinking_timer.is_stopped():
		var rng = randf_range(.4, 3.2)
		thinking_timer.wait_time = rng
		thinking_timer.start()
		return true
	return false


func _on_hit_box_area_entered(area):
	if area is Projectile or (area.name == "HurtBox" and area.get_parent() is Lover):
		if area is Projectile:
			area.queue_free()
		PV-=10
		$AnimationPlayer.play("take_hit")
		
	if PV<=0:
		$AnimationPlayer.play("transformation")
		await $AnimationPlayer.animation_finished
		
		#génération du lover
		var new_lover = preload("res://Characters/lover.tscn").instantiate()
		new_lover.global_position=global_position
		get_parent().add_child(new_lover)
		
		#register le lover si géré
		
		get_tree().current_scene.find_child("MobManager", true, false).call_deferred("register_mob",new_lover)
		
		#unregister le hater si pas géré
		get_tree().current_scene.find_child("MobManager", true, false).call_deferred("unregister_mob",self)
		#destruction du hater
		call_deferred("queue_free")
#
#func _on_hurtbox_area_detect():
#	cpt_ennemis+=1
#
#func _on_hurtbox_exit_detection():
#	cpt_ennemis-=1
	


func _on_anim_attack_timeout():
	$AnimationPlayer.play("atk")
	print("animationnnnnnn")


func _on_hurt_box_area_entered(area):
	if (area.get_parent() is Lover and area.name=="HitBox") or (area.get_parent() is Player and area.name=="HitBox"):
		cpt_ennemis+=1.


func _on_hurt_box_area_exited(area):
	if (area.get_parent() is Lover and area.name=="HitBox") or (area.get_parent() is Player and area.name=="HitBox"):
		cpt_ennemis-=1.
