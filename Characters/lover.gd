extends StaticBody2D
class_name Lover

#Wander: move randomly around (spawned in middle map)
#Defend: move around defense point (spawner)
#Pursue: follow one to fight
enum State {WANDER, DEFEND, PURSUE, FOLLOW_PLAYER}
var state = State.WANDER

var last_spawner_seen
var last_hater_seen
var target

var speed = 80
var direction = Vector2.ZERO
var thinking_timer

@export var PV_max = 20.0
var PV = PV_max

var point_follow

signal target_out_of_sight

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	$DetectionBox.connect("body_entered", Callable(self, "_on_body_detected"))
	$DetectionBox.connect("body_exited", Callable(self, "_on_body_exit_detection"))
	
	#TODO
	$HurtBox.connect("area_entered", Callable(self, "_on_hurtbox_area_detect"))
	$HurtBox.connect("area_exited", Callable(self, "_on_hurtbox_exit_detection"))
	
	target_out_of_sight.connect(_on_target_out_of_sight)
	
	thinking_timer = Timer.new()
	thinking_timer.one_shot = true
	add_child(thinking_timer)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	match state:
		State.WANDER:
			#TODO move randomly
			if can_think():
				var rng = randf_range(-PI, PI)
				direction = Vector2(cos(rng),sin(rng))
			position += direction * speed * delta
			look_for_someone_to_atk()
		State.DEFEND:
			#TODO move around the thing it is defending; staying near it
			look_for_someone_to_atk()
		State.PURSUE:
			position += (target.global_position - global_position).normalized() * speed * delta
		State.FOLLOW_PLAYER:
			position = target.global_position + point_follow

func change_state(s : State):
	state = s


func _on_body_detected(body):
	if body is Hater:
		last_hater_seen = body

func _on_body_exit_detection(body):
	if body is Hater:
		if body == last_hater_seen:
			last_hater_seen = null
	if body == target:
		emit_signal("target_out_of_sight")


func _on_area_detected(area):
	if area is Spawner and area.state == area.camp.HATER:
		last_spawner_seen = area

func _on_area_exit_detection(area):
	if area is Spawner and area.state == area.camp.HATER:
		if area == last_spawner_seen:
			last_spawner_seen = null
	if area == target:
		emit_signal("target_out_of_sight")


func _on_target_out_of_sight():
	if state == State.PURSUE:
		cancel_chase()


func look_for_someone_to_atk():
	#2/3 chance to target player over lover if lover is found too
	if last_spawner_seen:
		if (randi() % 3 > 0 and last_hater_seen) or not last_hater_seen:
			start_chase(last_spawner_seen)
	elif last_hater_seen:
		start_chase(last_hater_seen)
	

func start_chase(new_target):
	change_state(State.PURSUE)
	target = new_target

func cancel_chase():
	change_state(State.WANDER)
	target = null


func follow_player():
	change_state(State.FOLLOW_PLAYER)
	target = find_child("Player", true, false)
	var r = target.get_node("CollectBox/CollisionShape").shape.radius
	var rng = randf_range(-PI, PI)
	point_follow = Vector2(cos(rng),sin(rng)) * randi_range(10, r)
	


func can_think():
	if thinking_timer.is_stopped():
		var rng = randf_range(.4, 3.2)
		thinking_timer.wait_time = rng
		thinking_timer.start()
		return true
	return false


func _on_hit_box_area_entered(area):
	if area.name == "HurtBox" and area.get_parent() is Hater:
		PV -= 10
		$AnimationPlayer.play("take_hit")
	if area is Spawner and area.state == area.camp.HATER:
		PV -= 10
	if PV <= 0:
		get_tree().current_scene.find_child("MobManager", true, false).call_deferred("unregister_mob",self)
		call_deferred("queue_free")

